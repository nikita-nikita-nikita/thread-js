import callWebApi from 'src/helpers/webApiHelper';

export const addComment = async request => {
  const response = await callWebApi({
    endpoint: '/api/comments',
    type: 'POST',
    request
  });
  return response.json();
};

export const changeComment = async request => {
  const response = await callWebApi({
    endpoint: '/api/comments',
    type: 'PUT',
    request
  });
  return response.json();
};

export const getComment = async id => {
  const response = await callWebApi({
    endpoint: `/api/comments/${id}`,
    type: 'GET'
  });
  return response.json();
};

export const likeComment = async commentId => {
  const response = await callWebApi({
    endpoint: '/api/comments/react',
    type: 'PUT',
    request: {
      commentId,
      isLike: true,
      isDislike: false,
      template: 'like'
    }
  });
  return response.json();
};

export const dislikeComment = async commentId => {
  const response = await callWebApi({
    endpoint: '/api/comments/react',
    type: 'PUT',
    request: {
      commentId,
      isDislike: true,
      isLike: false,
      template: 'dislike'
    }
  });
  return response.json();
};

export const getAllComments = async postId => {
  const response = await callWebApi({
    endpoint: `/api/comments/?postId=${postId}`,
    type: 'GET'
  });
  return response.json();
};

export const getReactedPerson = async (commentId, reactType) => {
  const response = await callWebApi({
    endpoint: `/api/comments/react/?commentId=${commentId}&reactType=${reactType}`,
    type: 'GET'
  });
  return response.json();
};
