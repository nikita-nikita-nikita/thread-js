import callWebApi from 'src/helpers/webApiHelper';

export const sendNotificationError = async message => {
  const response = await callWebApi({
    endpoint: '/api/notification',
    type: 'POST',
    request: { message }
  });
  return response.json();
};
