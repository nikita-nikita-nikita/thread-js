import callWebApi from 'src/helpers/webApiHelper';

export const login = async request => {
  const response = await callWebApi({
    endpoint: '/api/auth/login',
    type: 'POST',
    request
  });
  return response.json();
};

export const changeProfile = async request => {
  const response = await callWebApi({
    endpoint: '/api/auth/user',
    type: 'PUT',
    request
  });
  return response.json();
};

export const registration = async request => {
  const response = await callWebApi({
    endpoint: '/api/auth/register',
    type: 'POST',
    request
  });
  return response.json();
};

export const getCurrentUser = async () => {
  try {
    const response = await callWebApi({
      endpoint: '/api/auth/user',
      type: 'GET'
    });
    return response.json();
  } catch (e) {
    return null;
  }
};

export const changePassword = async request => {
  const response = await callWebApi({
    endpoint: '/api/auth/change',
    type: 'PUT',
    request
  });
  return response.json();
};

export const resetPassword = async (newPassword, emailHash) => {
  const response = await callWebApi({
    endpoint: '/api/auth/reset',
    type: 'PUT',
    request: { newPassword, emailHash }
  });
  return response.json();
};
