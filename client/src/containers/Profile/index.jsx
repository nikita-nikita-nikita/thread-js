import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getUserImgLink } from 'src/helpers/imageHelper';
import validator from 'validator';
import {
  Grid,
  Image,
  Form,
  Button
} from 'semantic-ui-react';
import { bindActionCreators } from 'redux';
import { changeProfile } from './actions';
import styles from './styles.module.scss';
import { sendNotificationError } from '../../services/notificationsService';

const Profile = ({ user, changeProfile: change }) => {
  const [username, setUsername] = useState(user.username);
  const [email, setEmail] = useState(user.email);
  const [status, setStatus] = useState(user.status);
  const [isLoading, setIsLoading] = useState(false);
  const [isEmailValid, setIsEmailValid] = useState(true);
  const [newPassword, setNewPassword] = useState('');
  const [oldPassword, setOldPassword] = useState('');
  const submit = async () => {
    if (!isEmailValid || isLoading) {
      return;
    }
    setIsLoading(true);
    try {
      if (newPassword) await change({ email, username, status, oldPassword, newPassword });
      else await change({ email, username, status, oldPassword });
    } catch (error) {
      sendNotificationError(error.message);
    } finally {
      setIsLoading(false);
    }
  };
  return (
    <Grid container textAlign="center" style={{ paddingTop: 30 }}>
      <Grid.Column>
        <Image centered src={getUserImgLink(user.image)} size="medium" circular />
        <br />
        <Form className={styles.changeProfileForm} onSubmit={submit}>
          <Form.Input
            icon="user"
            iconPosition="left"
            placeholder="Username"
            type="text"
            value={username}
            onChange={({ target }) => setUsername(target.value)}
          />
          <br />
          <br />
          <Form.Input
            icon="at"
            iconPosition="left"
            placeholder="Email"
            type="email"
            value={email}
            error={!isEmailValid}
            onChange={({ target }) => setEmail(target.value)}
            onBlur={() => setIsEmailValid(validator.isEmail(email))}
          />
          <br />
          <br />
          <Form.Input
            icon="address card"
            iconPosition="left"
            placeholder="Status"
            type="text"
            value={status}
            onChange={({ target }) => setStatus(target.value)}
          />
          <br />
          <br />
          Enter new password
          <Form.Input
            icon="lock"
            iconPosition="left"
            placeholder="Enter new passport"
            type="password"
            value={newPassword}
            onChange={ev => setNewPassword(ev.target.value)}
          />
          <br />
          <br />
          Enter old password
          <Form.Input
            icon="lock"
            iconPosition="left"
            placeholder="Enter old passport"
            type="password"
            value={oldPassword}
            onChange={ev => setOldPassword(ev.target.value)}
          />
          <Button
            type="submit"
            color="teal"
            fluid
            size="large"
            style={{ width: '30%', margin: 'auto' }}
            loading={isLoading}
          >
            Change
          </Button>
        </Form>
      </Grid.Column>
    </Grid>
  );
};

Profile.propTypes = {
  user: PropTypes.objectOf(PropTypes.any),
  changeProfile: PropTypes.func.isRequired
};

Profile.defaultProps = {
  user: {}
};

const mapStateToProps = rootState => ({
  user: rootState.profile.user
});

const actions = { changeProfile };
const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Profile);
