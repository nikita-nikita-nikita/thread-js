import * as postService from 'src/services/postService';
import * as commentService from 'src/services/commentService';
import { reactComment, addCommentLogic, changeCommentLogic } from './actionsLogic/commentLogic';
import { reactPost } from './actionsLogic/postLogic';
import {
  ADD_POST,
  LOAD_MORE_POSTS,
  SET_ALL_POSTS,
  SET_EXPANDED_POST,
  CHANGE_POST
} from './actionTypes';

const setPostsAction = posts => ({
  type: SET_ALL_POSTS,
  posts
});

const addMorePostsAction = posts => ({
  type: LOAD_MORE_POSTS,
  posts
});

const addPostAction = post => ({
  type: ADD_POST,
  post
});

const setExpandedPostAction = post => ({
  type: SET_EXPANDED_POST,
  post
});

const changePostAction = changedPost => ({
  type: CHANGE_POST,
  changedPost
});

const addReactionFunc = service => async object => {
  const toReturn = { ...object };
  const likedPerson = await service.getReactedPerson(object.id, 'isLike');
  Object.assign(toReturn, { likedPerson });
  const dislikedPerson = await service.getReactedPerson(object.id, 'isDislike');
  Object.assign(toReturn, { dislikedPerson });
  return toReturn;
};

export const loadPosts = filter => async dispatch => {
  const posts = await postService.getAllPosts(filter);
  const postWithReactions = await posts.map(addReactionFunc(postService));
  dispatch(setPostsAction(await Promise.all(postWithReactions)));
};

export const loadMorePosts = filter => async (dispatch, getRootState) => {
  const { posts: { posts } } = getRootState();
  const loadedPosts = await postService.getAllPosts(filter);
  const filteredPosts = await (loadedPosts
    .filter(post => !(posts && posts.some(loadedPost => post.id === loadedPost.id))))
    .map(addReactionFunc(postService));
  dispatch(addMorePostsAction(await Promise.all(filteredPosts)));
};

export const applyPost = postId => async dispatch => {
  const post = await postService.getPost(postId);
  dispatch(addPostAction(post));
};

export const addPost = post => async dispatch => {
  const { id } = await postService.addPost(post);
  const newPost = await postService.getPost(id);
  dispatch(addPostAction(newPost));
};

export const changePost = changedPost => async dispatch => {
  const { ok, updatedPost } = await postService.changePost(changedPost);
  if (ok) dispatch(changePostAction(updatedPost));
};

export const toggleExpandedPost = postId => async dispatch => {
  const post = postId ? await postService.getPost(postId) : undefined;
  if (post) post.comments = await Promise.all(post.comments.map(addReactionFunc(commentService)));
  dispatch(setExpandedPostAction(post ? (await addReactionFunc(postService)(post)) : undefined));
};

export const likePost = reactPost(setExpandedPostAction, setPostsAction, postService, 'likePost');

export const dislikePost = reactPost(setExpandedPostAction, setPostsAction, postService, 'dislikePost');

export const addComment = addCommentLogic(setPostsAction, setExpandedPostAction, commentService);

export const changeComment = changeCommentLogic(setExpandedPostAction, commentService, postService);

export const likeComment = reactComment(setExpandedPostAction, commentService, 'likeComment');

export const dislikeComment = reactComment(setExpandedPostAction, commentService, 'dislikeComment');
