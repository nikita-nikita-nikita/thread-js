import { mapLikesAndDislikes, getDiffs } from './bothFunction';

export const reactComment = (
  setExpandedPostAction,
  commentService,
  template
) => commentId => async (
  dispatch,
  getRootState) => {
  const { newState, previousState } = await commentService[template](commentId);
  const { diffLike, diffDislike } = getDiffs(newState, previousState);
  const { posts: { expandedPost } } = getRootState();
  const updated = expandedPost.comments.map(com => (com.id !== commentId
    ? com
    : mapLikesAndDislikes(com, diffLike, diffDislike)));
  dispatch(setExpandedPostAction({ ...expandedPost, comments: [...updated] }));
};

export const addCommentLogic = (
  setPostsAction,
  setExpandedPostAction,
  commentService
) => request => async (dispatch, getRootState) => {
  const { id } = await commentService.addComment(request);
  const comment = await commentService.getComment(id);

  const mapComments = post => ({
    ...post,
    commentCount: Number(post.commentCount) + 1,
    comments: [...(post.comments || []), comment] // comment is taken from the current closure
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== comment.postId
    ? post
    : mapComments(post)));
  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === comment.postId) {
    dispatch(setExpandedPostAction(mapComments(expandedPost)));
  }
};

export const changeCommentLogic = (setExpandedPostAction, commentService, postService) => request => async dispatch => {
  const { id } = await commentService.changeComment(request);
  const newPost = id
    ? await postService.getPost(request.postId)
    : undefined;
  dispatch(setExpandedPostAction(newPost));
};
