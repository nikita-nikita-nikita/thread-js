export const mapLikesAndDislikes = (object, diffLike, diffDislike) => ({
  ...object,
  dislikeCount: Number(object.dislikeCount) + diffDislike,
  likeCount: Number(object.likeCount) + diffLike
});

export const getDiffs = (newState, previousState) => {
  let diffLike = newState.isLike ? 1 : -1;
  if (!(newState.isLike || previousState.isLike)) diffLike = 0;
  let diffDislike = newState.isDislike ? 1 : -1;
  if (!(newState.isDislike || previousState.isDislike)) diffDislike = 0;
  return { diffDislike, diffLike };
};
