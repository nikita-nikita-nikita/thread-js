import { mapLikesAndDislikes, getDiffs } from './bothFunction';

export const reactPost = (
  setExpandedPostAction,
  setPostsAction,
  postService,
  template
) => postId => async (dispatch, getRootState) => {
  const { newState, previousState } = await postService[template](postId);
  const { diffLike, diffDislike } = getDiffs(newState, previousState);
  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== postId ? post : mapLikesAndDislikes(post, diffLike, diffDislike)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(mapLikesAndDislikes(expandedPost, diffLike, diffDislike)));
  }
};
