/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import * as imageService from 'src/services/imageService';
import ExpandedPost from 'src/containers/ExpandedPost';
import Post from 'src/components/Post';
import AddPost from 'src/components/AddPost';
import SharedPostLink from 'src/components/SharedPostLink';
import { Checkbox, Loader } from 'semantic-ui-react';
import InfiniteScroll from 'react-infinite-scroller';
import { loadPosts, loadMorePosts, likePost, dislikePost, toggleExpandedPost, addPost, changePost } from './actions';

import styles from './styles.module.scss';

const postsFilter = {
  userId: undefined,
  noUserId: '00000000-0000-0000-0000-000000000000',
  from: 0,
  count: 10,
  currentUserId: undefined,
  showByLike: undefined
};

const Thread = ({
  userId,
  loadPosts: load,
  loadMorePosts: loadMore,
  posts = [],
  expandedPost,
  hasMorePosts,
  addPost: createPost,
  likePost: like,
  dislikePost: dislike,
  changePost: change,
  toggleExpandedPost: toggle
}) => {
  const [sharedPostId, setSharedPostId] = useState(undefined);
  const [showOwnPosts, setShowOwnPosts] = useState(false);
  const [showOthersPost, setShowOthersPosts] = useState(false);
  const [showLikedPost, setShowLikedPosts] = useState(false);

  const toggleShowOwnPosts = ({ target }) => {
    setShowOwnPosts(!showOwnPosts);
    postsFilter.userId = showOwnPosts ? undefined : userId;
    postsFilter.from = 0;
    load(postsFilter);
    postsFilter.from = postsFilter.count; // for the next scroll
  };

  useEffect(() => {
    postsFilter.currentUserId = userId;
  });

  const toggleShowOthersPosts = ({ target }) => {
    postsFilter.noUserId = !showOthersPost ? userId : '00000000-0000-0000-0000-000000000000';
    postsFilter.from = 0;
    load(postsFilter);
    postsFilter.from = postsFilter.count;
    setShowOthersPosts(!showOthersPost);
  };
  // console.log(posts.filter(post => !post.isDelete));
  const toggleShowLikedPosts = ({ target }) => {
    postsFilter.showByLike = !showLikedPost || undefined;
    postsFilter.from = 0;
    load(postsFilter);
    postsFilter.from = postsFilter.count;
    setShowLikedPosts(!showLikedPost);
  };
  const getMorePosts = () => {
    loadMore(postsFilter);
    const { from, count } = postsFilter;
    postsFilter.from = from + count;
  };

  const sharePost = id => {
    setSharedPostId(id);
  };
  const uploadImage = file => imageService.uploadImage(file);
  const changeImage = (file, currentImage) => imageService.changeImage(file, currentImage);
  return (
    <div className={styles.threadContent}>
      <div className={styles.addPostForm}>
        <AddPost addPost={createPost} uploadImage={uploadImage} />
      </div>
      <div className={styles.toolbar}>
        <Checkbox
          toggle
          label="Show only my posts"
          checked={showOwnPosts}
          onChange={toggleShowOwnPosts}
        />
        <br />
        <Checkbox
          toggle
          label="Show only not my posts"
          checked={showOthersPost}
          onChange={toggleShowOthersPosts}
        />
        <br />
        <Checkbox
          toggle
          label="Show only liked posts"
          checked={showLikedPost}
          onChange={toggleShowLikedPosts}
        />
      </div>
      <InfiniteScroll
        pageStart={0}
        loadMore={getMorePosts}
        hasMore={hasMorePosts}
        loader={<Loader active inline="centered" key={0} />}
      >
        {(posts.filter(post => !post.isDelete)).map(post => (
          <Post
            post={post}
            likePost={like}
            dislikePost={dislike}
            toggleExpandedPost={toggle}
            sharePost={sharePost}
            changePost={change}
            key={post.id}
            userId={userId}
            changeImage={changeImage}
          />
        ))}
      </InfiniteScroll>
      {expandedPost && <ExpandedPost changeImage={changeImage} sharePost={sharePost} />}
      {sharedPostId && <SharedPostLink postId={sharedPostId} close={() => setSharedPostId(undefined)} />}
    </div>
  );
};

Thread.propTypes = {
  posts: PropTypes.arrayOf(PropTypes.object),
  hasMorePosts: PropTypes.bool,
  expandedPost: PropTypes.objectOf(PropTypes.any),
  userId: PropTypes.string,
  loadPosts: PropTypes.func.isRequired,
  loadMorePosts: PropTypes.func.isRequired,
  likePost: PropTypes.func.isRequired,
  dislikePost: PropTypes.func.isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  addPost: PropTypes.func.isRequired,
  changePost: PropTypes.func.isRequired
};

Thread.defaultProps = {
  posts: [],
  hasMorePosts: true,
  expandedPost: undefined,
  userId: undefined
};

const mapStateToProps = rootState => ({
  posts: rootState.posts.posts,
  hasMorePosts: rootState.posts.hasMorePosts,
  expandedPost: rootState.posts.expandedPost,
  userId: rootState.profile.user.id
});

const actions = {
  loadPosts,
  loadMorePosts,
  likePost,
  dislikePost,
  changePost,
  toggleExpandedPost,
  addPost
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Thread);
