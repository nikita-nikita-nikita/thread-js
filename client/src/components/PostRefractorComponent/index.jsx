import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Form, Button, Icon, Image, Segment } from 'semantic-ui-react';
import styles from './styles.module.scss';
import { sendNotificationError } from '../../services/notificationsService';

const WorkWithPost = ({
  postServiceFunc,
  imageServiceFunc,
  startBody = '',
  startImage = {},
  currentPost = {},
  requestRender
}) => {
  const [body, setBody] = useState(startBody);
  const [image, setImage] = useState(startImage);
  const [isUploading, setIsUploading] = useState(false);

  const handleAddPost = async () => {
    if (!body) {
      return;
    }
    await postServiceFunc(requestRender(image, body, currentPost));
    setBody('');
    setImage(undefined);
  };

  const handleUploadFile = async ({ target }) => {
    setIsUploading(true);
    try {
      const { id, link } = await imageServiceFunc(target.files[0]);
      setImage({ id, link });
    } catch (error) {
      sendNotificationError(error.message);
    } finally {
      setIsUploading(false);
    }
  };
  const DeleteButton = () => (
    <Button color="red" onClick={() => setImage(null)}>
      <Icon name="delete" />
      Delete image
    </Button>
  );
  return (
    <Segment>
      <Form onSubmit={handleAddPost}>
        <Form.TextArea
          name="body"
          value={body}
          placeholder="What is the news?"
          onChange={ev => setBody(ev.target.value)}
        />
        {image?.link && (
          <div className={styles.imageWrapper}>
            <Image className={styles.image} src={image?.link} alt="post" />
          </div>
        )}
        <Button color="teal" icon labelPosition="left" as="label" loading={isUploading} disabled={isUploading}>
          <Icon name="image" />
          Attach image
          <input name="image" type="file" onChange={handleUploadFile} hidden />
        </Button>
        {
          (image && image.id)
            ? <DeleteButton />
            : undefined
        }
        <Button floated="right" color="blue" type="submit">Post</Button>
      </Form>
    </Segment>
  );
};

WorkWithPost.propTypes = {
  postServiceFunc: PropTypes.func.isRequired,
  imageServiceFunc: PropTypes.func.isRequired,
  requestRender: PropTypes.func.isRequired,
  startBody: PropTypes.string,
  startImage: PropTypes.objectOf(PropTypes.any),
  currentPost: PropTypes.objectOf(PropTypes.any)
};

WorkWithPost.defaultProps = {
  startBody: '',
  startImage: {},
  currentPost: {}
};

export default WorkWithPost;
