import React from 'react';
import PropTypes from 'prop-types';
import WorkWithPost from '../PostRefractorComponent';

const requestRender = (image, body, currentPost) => (
  { ...currentPost,
    image,
    body
  });
const ChangePost = ({
  changePost,
  changeImage,
  startBody,
  startImage = {},
  currentPost = {}
}) => (
  <WorkWithPost
    imageServiceFunc={changeImage}
    postServiceFunc={changePost}
    startBody={startBody}
    startImage={startImage}
    currentPost={currentPost}
    requestRender={requestRender}
  />
);

ChangePost.propTypes = {
  changePost: PropTypes.func.isRequired,
  changeImage: PropTypes.func.isRequired,
  startBody: PropTypes.string.isRequired,
  startImage: PropTypes.objectOf(PropTypes.any),
  currentPost: PropTypes.objectOf(PropTypes.any)
};

ChangePost.defaultProps = {
  startImage: {},
  currentPost: {}
};

export default ChangePost;
