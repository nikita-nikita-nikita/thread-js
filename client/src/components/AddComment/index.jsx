import React from 'react';
import PropTypes from 'prop-types';
import WorkWithComment from '../CommentRefractorCommponent';

const AddComment = ({ postId, addComment }) => (
  <WorkWithComment postId={postId} commentServiceFunc={addComment} />
);

AddComment.propTypes = {
  addComment: PropTypes.func.isRequired,
  postId: PropTypes.string.isRequired
};

export default AddComment;
