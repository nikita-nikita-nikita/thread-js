import React, { useState } from 'react';
import PropTypes from 'prop-types';
import Avatar from '@material-ui/core/Avatar';
import { Modal } from 'semantic-ui-react';
import style from './styles.module.scss';
import { getUserImgLink } from '../../helpers/imageHelper';

const ShowReactedPerson = ({
  reactedPerson
}) => {
  const [isOpenModal, setIsOpenModal] = useState(false);
  if (!reactedPerson || reactedPerson.length === 0) return <></>;
  const count = (reactedPerson.length > 3) ? 3 : reactedPerson.length;
  const openModal = ev => {
    ev.preventDefault();
    setIsOpenModal(true);
  };
  const mapPerson = reactedPerson.map((person, index) => ((index + 1 <= count)
    ? <Avatar alt={person.username} src={getUserImgLink(person)} className={style.smallAvatar} onClick={openModal} />
    : null));
  const modal = isOpenModal ? (
    <Modal dimmer="blurring" size="mini" open={isOpenModal} onClose={() => setIsOpenModal(false)}>
      {reactedPerson.map(person => (
        <div className={style.fullUser}>
          <Avatar circular src={getUserImgLink(person)} className={style.bigAvatar} />
          <span>{person.username}</span>
        </div>
      ))}
    </Modal>
  ) : null;
  return (
    <>
      {mapPerson}
      {modal}
    </>
  );
};

ShowReactedPerson.propTypes = {
  reactedPerson: PropTypes.arrayOf(PropTypes.object).isRequired
};

export default ShowReactedPerson;
