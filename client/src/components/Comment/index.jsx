import React, { useState } from 'react';
import Spacer from 'react-spacer';
import PropTypes from 'prop-types';
import { Comment as CommentUI, Icon, Label } from 'semantic-ui-react';
import moment from 'moment';
import { getUserImgLink } from 'src/helpers/imageHelper';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import WorkWithComment from '../CommentRefractorCommponent';
import { changeComment, likeComment, dislikeComment } from '../../containers/Thread/actions';
import styles from './styles.module.scss';
import ShowReactedPerson from '../ShowReactedPerson';

const Comment = ({
  comment: { body, postId, user, createdAt, id, isDelete, likeCount, dislikeCount, likedPerson, dislikedPerson },
  userId,
  changeComment: change,
  likeComment: like,
  dislikeComment: dislike }) => {
  const [currentBody, setCurrentBody] = useState(body);
  const [isCommentChanged, setIsCommentChanged] = useState(false);
  const isPostOwner = userId === user.id;
  const updateComment = ({ body: newBody }) => {
    setCurrentBody(newBody);
    setIsCommentChanged(false);
    change({ id, body: newBody, userId: user.id, postId });
  };
  const doubleClickFunction = (isPostOwner) ? () => setIsCommentChanged(true) : null;
  const showToPostOwner = (isPostOwner)
    ? (
      <>
        <Label
          basic
          size="small"
          style={{ border: 'none' }}
          as="a"
          onClick={() => setIsCommentChanged(true)}
        >
          <Icon name="edit outline" />
        </Label>
        <Label
          basic
          size="small"
          as="a"
          style={{ border: 'none' }}
          onClick={() => change({ id, isDelete: true, userId: user.id, postId })}
        >
          <Icon name="trash" />
        </Label>
      </>
    )
    : null;
  if (isDelete) return null;
  return isCommentChanged
    ? <WorkWithComment postId={postId} commentServiceFunc={updateComment} currentBody={currentBody} />
    : (
      <CommentUI className={styles.comment} onDoubleClick={doubleClickFunction}>
        <CommentUI.Avatar src={getUserImgLink(user.image)} />
        <CommentUI.Content>
          <CommentUI.Author as="a">
            {user.username}
          </CommentUI.Author>
          <CommentUI.Metadata>
            {moment(createdAt).fromNow()}
          </CommentUI.Metadata>
          <CommentUI.Text>
            {currentBody}
          </CommentUI.Text>
          <CommentUI.Action style={{ display: 'flex' }}>
            <Label basic size="small" as="a" style={{ border: 'none' }} onClick={() => like(id)}>
              <Icon name="thumbs up" />
              {likeCount}
            </Label>
            <ShowReactedPerson reactedPerson={likedPerson} />
            <Label basic size="small" as="a" style={{ border: 'none' }} onClick={() => dislike(id)}>
              <Icon name="thumbs down" />
              {dislikeCount}
            </Label>
            <ShowReactedPerson reactedPerson={dislikedPerson} />
            <Spacer grow="2" />
            {showToPostOwner}
          </CommentUI.Action>
        </CommentUI.Content>
      </CommentUI>
    );
};

Comment.propTypes = {
  comment: PropTypes.objectOf(PropTypes.any).isRequired,
  userId: PropTypes.string.isRequired,
  changeComment: PropTypes.func.isRequired,
  likeComment: PropTypes.func.isRequired,
  dislikeComment: PropTypes.func.isRequired
};

const mapStateToProps = rootState => ({
  userId: rootState.profile.user.id
});

const actions = {
  changeComment,
  likeComment,
  dislikeComment
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);
export default connect(mapStateToProps, mapDispatchToProps)(Comment);
