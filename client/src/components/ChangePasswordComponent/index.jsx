import React, { useState } from 'react';
import { Button, Form, Grid } from 'semantic-ui-react';
import validator from 'validator';
import Logo from 'src/components/Logo';
import styles from './styles.module.scss';
import { sendNotificationError } from '../../services/notificationsService';
import { changePassword } from '../../services/authService';

const ChangePasswordComponent = () => {
  const [email, setEmail] = useState('');
  const [isEmailValid, setEmailValid] = useState(true);
  const [isLoading, setIsLoading] = useState(false);
  const [message, setMessage] = useState('');
  const emailChanged = value => {
    setEmail(value);
    setEmailValid(true);
  };
  const submit = async () => {
    if (!isEmailValid || isLoading) {
      return;
    }
    setIsLoading(true);
    try {
      const result = await changePassword({ email });
      setMessage(result.message);
    } catch (error) {
      sendNotificationError(error.message);
    } finally {
      setIsLoading(false);
    }
  };
  return (
    <Grid container textAlign="center" style={{ paddingTop: '30rem' }}>
      <Grid.Column>
        <Logo />
        <Form className={styles.resetProfileComponent} onSubmit={submit}>
          <br />
          <br />
          Enter your account email and username
          <Form.Input
            icon="at"
            iconPosition="left"
            placeholder="Enter email"
            value={email}
            error={!isEmailValid}
            onChange={({ target }) => emailChanged(target.value)}
            onBlur={() => setEmailValid(validator.isEmail(email))}
          />
          <Button
            type="submit"
            color="teal"
            fluid
            size="large"
            style={{ width: '30%', margin: 'auto' }}
            loading={isLoading}
          >
            Change
          </Button>
        </Form>
        {message}
        <br />
        <br />
      </Grid.Column>
    </Grid>
  );
};

export default ChangePasswordComponent;
