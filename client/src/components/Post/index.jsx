import React, { Fragment, useState } from 'react';
import PropTypes from 'prop-types';
import { Card, Image, Label, Icon } from 'semantic-ui-react';
import moment from 'moment';
// eslint-disable-next-line no-unused-vars
import ShowReactedPerson from '../ShowReactedPerson';

import ChangePost from '../ChangePost';
import styles from './styles.module.scss';
/* eslint-disable*/
const Post = ({ post, likePost, dislikePost, toggleExpandedPost, sharePost, changePost, userId, changeImage }) => {
  const {
    id,
    image,
    body,
    user,
    likeCount,
    dislikeCount,
    commentCount,
    createdAt,
    likedPerson,
    dislikedPerson
  } = post;
  const [isShowPostChangeArea, setIsShowTextArea] = useState(false);
  const [newBody, setNewBody] = useState(body);
  const [currentImage, setCurrentImage] = useState(image);
  const date = moment(createdAt).fromNow();
  const updatePost = () => async newPost => {
    setIsShowTextArea(false);
    setNewBody(newPost.body);
    setCurrentImage({ id: newPost.image.id, link: newPost.image.link });
    return changePost(newPost);
  };
  const updateImage = () => async file => changeImage(file, image);
  const isPostOwner = userId === user.id;
  const doubleClickFunction = (isPostOwner) ? () => setIsShowTextArea(true) : null;
  const showToPostOwner = (isPostOwner) ? (
    <>
      <Label
        basic
        size="small"
        as="a"
        className={styles.toolbarBtn}
        onClick={() => setIsShowTextArea(true)}
      >
        <Icon name="edit outline" />
      </Label>
      <Label
        basic
        size="small"
        as="a"
        className={styles.toolbarBtn}
        onClick={() => changePost({ ...post, isDelete: true })}
      >
        <Icon name="trash" />
      </Label>
    </>
  ) : null;
  return (
    // eslint-disable-next-line
    <Fragment>
      {!isShowPostChangeArea
        ? (
          <Card style={{ width: '100%' }}>
            {currentImage && <Image src={currentImage.link} wrapped ui={false} />}
            <Card.Content>
              <Card.Meta>
                <span className="date">
                  posted by
                  {' '}
                  {user.username}
                  {' - '}
                  {date}
                </span>
              </Card.Meta>
              <Card.Description onDoubleClick={doubleClickFunction}>
                <p>{newBody}</p>
              </Card.Description>
            </Card.Content>
            <Card.Content extra style={{ display: 'flex', flexDirection: 'row' }}>
              <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => likePost(id)}>
                <Icon name="thumbs up" />
                {likeCount}
              </Label>
              <ShowReactedPerson reactedPerson={likedPerson}/>
              <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => dislikePost(id)}>
                <Icon name="thumbs down" />
                {dislikeCount}
              </Label>
              <ShowReactedPerson reactedPerson={dislikedPerson} />
              <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => toggleExpandedPost(id)}>
                <Icon name="comment" />
                {commentCount}
              </Label>
              <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => sharePost(id)}>
                <Icon name="share alternate" />
              </Label>
              {showToPostOwner}
            </Card.Content>
          </Card>
        )
        : (
          <ChangePost
            startBody={newBody}
            changePost={updatePost()}
            changeImage={updateImage()}
            startImage={currentImage}
            currentPost={post}
          />
        )}
    </Fragment>
  );
};

Post.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  likePost: PropTypes.func.isRequired,
  dislikePost: PropTypes.func.isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired,
  changePost: PropTypes.func.isRequired,
  userId: PropTypes.string.isRequired,
  changeImage: PropTypes.func.isRequired
};

export default Post;
