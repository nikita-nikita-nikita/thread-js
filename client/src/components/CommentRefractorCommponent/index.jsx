import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Form, Button } from 'semantic-ui-react';

const WorkWithComment = ({
  postId,
  commentServiceFunc,
  currentBody
}) => {
  const [body, setBody] = useState(currentBody);

  const handleAddComment = async () => {
    if (!body) {
      return;
    }
    await commentServiceFunc({ postId, body });
    setBody('');
  };

  return (
    <Form reply onSubmit={handleAddComment}>
      <Form.TextArea
        value={body}
        placeholder="Type a comment..."
        onChange={ev => setBody(ev.target.value)}
      />
      <Button type="submit" content="Post comment" labelPosition="left" icon="edit" primary />
    </Form>
  );
};

WorkWithComment.propTypes = {
  commentServiceFunc: PropTypes.func.isRequired,
  postId: PropTypes.string.isRequired,
  currentBody: PropTypes.string
};

WorkWithComment.defaultProps = {
  currentBody: ''
};

export default WorkWithComment;
