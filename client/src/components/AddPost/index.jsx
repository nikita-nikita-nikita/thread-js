import React from 'react';
import PropTypes from 'prop-types';
import WorkWithPost from '../PostRefractorComponent';

const requestRender = (image, body) => ({ imageId: image?.id, body });

const AddPost = ({
  addPost,
  uploadImage
}) => <WorkWithPost imageServiceFunc={uploadImage} postServiceFunc={addPost} requestRender={requestRender} />;

AddPost.propTypes = {
  addPost: PropTypes.func.isRequired,
  uploadImage: PropTypes.func.isRequired
};

export default AddPost;
