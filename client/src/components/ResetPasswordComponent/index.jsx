import React, { useState } from 'react';
import PropTypes from 'prop-types';
import Logo from 'src/components/Logo';
import { Button, Form, Grid, Header } from 'semantic-ui-react';
import { resetPassword } from '../../services/authService';

const ResetPassword = ({ match }) => {
  const [password, setPassword] = useState('');
  const [isPasswordValid, setPasswordValid] = useState(true);
  const [isLoading, setLoading] = useState(false);
  const [message, setMessage] = useState('');
  const passwordChanged = value => {
    setPassword(value);
    setPasswordValid(true);
  };
  const submit = async ev => {
    ev.preventDefault();
    setLoading(true);
    try {
      const { message: mes } = await resetPassword(password, match.params.resetHash.slice(1));
      setMessage(mes);
    } catch (error) {
      setMessage(error.message);
    } finally {
      setLoading(false);
    }
  };
  return (
    <Grid textAlign="center" verticalAlign="middle" className="fill">
      <Grid.Column style={{ maxWidth: 450 }}>
        <Logo />
        <Header as="h2" color="teal" textAlign="center">
          Enter new password
        </Header>
        <Form onSubmit={submit}>
          <Form.Input
            fluid
            icon="lock"
            iconPosition="left"
            placeholder="Password"
            type="password"
            error={!isPasswordValid}
            onChange={ev => passwordChanged(ev.target.value)}
            onBlur={() => setPasswordValid(Boolean(password))}
          />
          <Button type="submit" loading={isLoading}>Submit</Button>
        </Form>
        {message}
      </Grid.Column>
    </Grid>
  );
};

ResetPassword.propTypes = {
  match: PropTypes.objectOf(PropTypes.any)
};

ResetPassword.defaultProps = {
  match: undefined
};

export default ResetPassword;
