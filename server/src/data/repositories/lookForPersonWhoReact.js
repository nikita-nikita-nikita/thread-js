import sequelize from '../db/connection';
import imageRepository from './imageRepository';

const searchReactionQuery = (reactType, reactName, id) => (`
(SELECT "users"."imageId", "users"."username"
FROM "${reactName}s"
INNER JOIN "${reactName}Reactions" ON
"${reactName}Reactions"."${reactName}Id" = "${reactName}s"."id"
INNER JOIN "users" ON
"users"."id" = "${reactName}Reactions"."userId"
WHERE "${reactName}s"."isDelete" = 'false'
AND "${reactName}s"."id" = '${id}'
AND "${reactName}Reactions"."${reactType}" = 'true')`);

const getReactedByQuery = async query => {
  const result = await (sequelize.query(query, { type: sequelize.QueryTypes.SELECT }));
  const withImages = result.map(async ({ username, imageId }) => {
    const link = (imageId) ? (await imageRepository.getById(imageId)).dataValues.link : null;
    return { username, link };
  });
  return Promise.all(withImages);
};

export const getReactedPostPerson = (
  postId,
  reactType
) => getReactedByQuery(searchReactionQuery(reactType, 'post', postId));

export const getReactedCommentPerson = async (
  commentId,
  reactType
) => getReactedByQuery(searchReactionQuery(reactType, 'comment', commentId));
