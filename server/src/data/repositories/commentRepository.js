import { CommentModel, UserModel, ImageModel } from '../models/index';
import BaseRepository from './baseRepository';
import sequelize from '../db/connection';

const querySearchById = (id, template) => (`(
                      SELECT COUNT(*)
                      FROM comments
                      INNER JOIN "commentReactions"
                       ON comments.id = "commentReactions"."commentId"
                      AND "commentReactions"."${template}" = 'true'
                      AND comments.id = '${id}')
`);

class CommentRepository extends BaseRepository {
  async getCommentById(id) {
    return this.model.findOne({
      group: [
        'comment.id',
        'user.id',
        'user->image.id'
      ],
      attributes: {
        include: [
          [sequelize.literal(querySearchById(id, 'isLike')), 'likeCount'],
          [sequelize.literal(querySearchById(id, 'isDislike')), 'dislikeCount']
        ]
      },
      where: { id },
      include: [{
        model: UserModel,
        attributes: ['id', 'username'],
        include: {
          model: ImageModel,
          attributes: ['id', 'link']
        }
      }]
    });
  }
}

export default new CommentRepository(CommentModel);
