import BaseRepository from './baseRepository';
import { ResetPasswordModel } from '../models/index';

class ResetPasswordRepository extends BaseRepository {
  // eslint-disable-next-line no-unused-vars
  async getByEmail(email) {
    return this.model.findOne({ where: { email } });
  }

  async getByHash(hash) {
    return this.model.findOne({ where: { hash } });
  }
}

export default new ResetPasswordRepository(ResetPasswordModel);
