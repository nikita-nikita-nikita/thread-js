import sequelize from '../db/connection';
import { PostModel, CommentModel, UserModel, ImageModel, PostReactionModel } from '../models/index';
import BaseRepository from './baseRepository';

const querySearch = template => (`
(SELECT COUNT(*)
FROM "commentReactions" as "react"
WHERE "react"."commentId" = "comments"."id"
AND "comments"."isDelete" = 'false'
AND "react"."${template}" = 'true')
`);
const likeOrDislikeCase = template => `CASE WHEN "postReactions"."${template}" = 'true' THEN 1 ELSE 0 END`;
class PostRepository extends BaseRepository {
  async getPosts(filter) {
    const {
      from: offset,
      count: limit,
      userId,
      noUserId,
      showByLike,
      currentUserId
    } = filter;

    const where = { isDelete: false };
    if (userId) {
      Object.assign(where, { userId });
    }
    const showByLikeQueryString = showByLike
      ? `
                        INNER JOIN "postReactions" ON
                        "posts"."id" = "postReactions"."postId" AND
                        "postReactions"."userId" = '${currentUserId}'
                         AND "postReactions"."isLike" = 'true'`
      : '';
    // look for correct ids in DB
    const resultFromDB = await sequelize.query(`(
                        SELECT id
                        FROM "posts"
                        WHERE "posts"."userId" <> '${noUserId}'
                        INTERSECT
                        SELECT posts.id
                        FROM posts ${showByLikeQueryString})`, { type: sequelize.QueryTypes.SELECT });
    return this.model.findAll({
      where,
      attributes: {
        include: [
          [sequelize.literal(`
                        (SELECT COUNT(*)
                        FROM "comments" as "comment"
                        WHERE "post"."id" = "comment"."postId" AND "comment"."isDelete" = 'false')`), 'commentCount'],
          [sequelize.fn('SUM', sequelize.literal(likeOrDislikeCase('isLike'))), 'likeCount'],
          [sequelize.fn('SUM', sequelize.literal(likeOrDislikeCase('isDislike'))), 'dislikeCount']
        ]
      },
      include: [{
        model: ImageModel,
        attributes: ['id', 'link']
      }, {
        model: UserModel,
        attributes: ['id', 'username'],
        include: {
          model: ImageModel,
          attributes: ['id', 'link']
        }
      }, {
        model: PostReactionModel,
        attributes: [],
        duplicating: false
      }],
      group: [
        'post.id',
        'image.id',
        'user.id',
        'user->image.id'
      ],
      order: [['createdAt', 'DESC']],
      offset,
      limit
    }).then(result => result.filter(post => resultFromDB.findIndex(id => post.dataValues.id === id.id) !== -1));
  }

  getPostById(id) {
    return this.model.findOne({
      group: [
        'post.id',
        'comments.id',
        'comments->user.id',
        'comments->user->image.id',
        'user.id',
        'user->image.id',
        'image.id'
      ],
      where: { id },
      attributes: {
        include: [
          [sequelize.literal(`
                        (SELECT COUNT(*)
                        FROM "comments" as "comment"
                        WHERE "post"."id" = "comment"."postId" AND "comment"."isDelete" = 'false')`), 'commentCount'],
          [sequelize.fn('SUM', sequelize.literal(likeOrDislikeCase('isLike'))), 'likeCount'],
          [sequelize.fn('SUM', sequelize.literal(likeOrDislikeCase('isDislike'))), 'dislikeCount']
        ]
      },
      include: [{
        model: CommentModel,
        attributes: {
          include: [
            [sequelize.literal(querySearch('isLike')), 'likeCount'],
            [sequelize.literal(querySearch('isDislike')), 'dislikeCount']
          ]
        },
        include: {
          model: UserModel,
          attributes: ['id', 'username'],
          include: {
            model: ImageModel,
            attributes: ['id', 'link']
          }
        }
      }, {
        model: UserModel,
        attributes: ['id', 'username'],
        include: {
          model: ImageModel,
          attributes: ['id', 'link']
        }
      }, {
        model: ImageModel,
        attributes: ['id', 'link']
      }, {
        model: PostReactionModel,
        attributes: []
      }]
    });
  }
}

export default new PostRepository(PostModel);
