export default (orm, DataTypes) => {
  const ResetPassword = orm.define('resetPasswords', {
    email: DataTypes.TEXT,
    hash: DataTypes.TEXT,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE
  }, {});

  return ResetPassword;
};
