import orm from '../db/connection';
import associate from '../db/associations';

const User = orm.import('./user');
const Post = orm.import('./post');
const PostReaction = orm.import('./postReaction');
const Comment = orm.import('./comment');
const Image = orm.import('./image');
const ResetPassword = orm.import('./resetPassword');
const CommentReaction = orm.import('./commentReaction');
associate({
  User,
  Post,
  PostReaction,
  Comment,
  Image,
  ResetPassword,
  CommentReaction
});

export {
  User as UserModel,
  Post as PostModel,
  PostReaction as PostReactionModel,
  CommentReaction as CommentReactionModel,
  Comment as CommentModel,
  Image as ImageModel,
  ResetPassword as ResetPasswordModel
};
