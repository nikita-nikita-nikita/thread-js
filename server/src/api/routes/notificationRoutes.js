import { Router } from 'express';

const router = Router();

router
  .post('/', (req, res) => {
    req.io.to(req.user.id).emit('error', req.body.message);
    res.send({ ok: true });
  });

export default router;
