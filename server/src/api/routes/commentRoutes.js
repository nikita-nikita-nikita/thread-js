import { Router } from 'express';
import * as commentService from '../services/commentService';
import { getPersonWhoReactOnComment } from '../services/reactionService';

const router = Router();

router
  .get('/', (req, res, next) => commentService.getAllCommentsFromPost(req.query.postId)
    .then(comments => res.send(comments))
    .catch(next))
  .get('/react/', (req, res, next) => getPersonWhoReactOnComment(req.query.commentId, req.query.reactType)
    .then(person => res.send(person))
    .catch(next))
  .get('/:id', (req, res, next) => commentService.getCommentById(req.params.id)
    .then(comment => res.send(comment))
    .catch(next))
  .post('/', (req, res, next) => commentService.create(req.user.id, req.body)
    .then(comment => res.send(comment))
    .catch(next))
  .put('/', (req, res, next) => commentService.update(req.user.id, req.body)
    .then(comment => res.send(comment))
    .catch(next))
  .put('/react', (req, res, next) => commentService.setReaction(req.user.id, req.body)
    .then(react => res.send(react))
    .catch(next));

export default router;
