import { Router } from 'express';
import * as authService from '../services/authService';
import * as userService from '../services/userService';
import authenticationMiddleware from '../middlewares/authenticationMiddleware';
import registrationMiddleware from '../middlewares/registrationMiddleware';
import jwtMiddleware from '../middlewares/jwtMiddleware';
import changeProfileMiddleware from '../middlewares/changeProfileMiddleware';

const router = Router();
// user added to the request (req.user) in a strategy, see passport config
router
  .put('/change', (req, res, next) => authService.changePassword(req.body.email)
    .then(response => res.send(response))
    .catch(next))
  .put('/reset', (req, res, next) => authService.resetPassword(req.body.newPassword, req.body.emailHash)
    .then(response => res.send(response))
    .catch(next))
  .post('/login', authenticationMiddleware, (req, res, next) => authService.login(req.user)
    .then(data => res.send(data))
    .catch(next))
  .post('/register', registrationMiddleware, (req, res, next) => authService.register(req.user)
    .then(data => res.send(data))
    .catch(next))
  .get('/user', jwtMiddleware, (req, res, next) => userService.getUserById(req.user.id)
    .then(data => res.send(data))
    .catch(next))
  .put('/user', changeProfileMiddleware, (req, res, next) => authService.changeUser(req.user.id, req.body)
    .then(data => res.send(data))
    .catch(next));

export default router;
