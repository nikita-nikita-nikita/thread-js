import { Router } from 'express';
import * as postService from '../services/postService';
import { getPersonWhoReactOnPost } from '../services/reactionService';
import { sendListById } from '../services/emailService';

const router = Router();

router
  .get('/', (req, res, next) => postService.getPosts(req.query)
    .then(posts => {
      res.send(posts);
    })
    .catch(next))
  .get('/react/', (req, res, next) => getPersonWhoReactOnPost(req.query.postId, req.query.reactType)
    .then(person => res.send(person))
    .catch(next))
  .get('/:id', (req, res, next) => postService.getPostById(req.params.id)
    .then(post => res.send(post))
    .catch(next))
  .post('/', (req, res, next) => postService.create(req.user.id, req.body)
    .then(post => {
      req.io.emit('new_post', post); // notify all users that a new post was created
      return res.send(post);
    })
    .catch(next))
  .put('/', (req, res, next) => postService.changePost(req.body.id, req.body, req.user.id)
    .then(updatedPost => (updatedPost
      ? res.send({ updatedPost, ok: true })
      : res.send({ ok: false })))
    .catch(next))
  .put('/react', (req, res, next) => postService.setReaction(req.user.id, req.body)
    .then(reaction => {
      if (reaction.post
        && reaction.post.userId !== req.user.id
        && (reaction.newState.isLike || reaction.newState.isDislike)) {
        // notify a user if someone (not himself) liked his post
        req.io.to(reaction.post.userId).emit(req.body.template, `Your post was ${req.body.template}d!`);
        sendListById(`Your post was ${req.body.template}d!`, reaction.post.userId);
      }
      return res.send(reaction);
    })
    .catch(next));
export default router;
