import sha256 from 'js-sha256';
import { createToken } from '../../helpers/tokenHelper';
import { encrypt } from '../../helpers/cryptoHelper';
import userRepository from '../../data/repositories/userRepository';
import { sendListByEmail } from './emailService';
import resetPasswordRepository from '../../data/repositories/resetPasswordRepository';

export const login = async ({ id }) => ({
  token: createToken({ id }),
  user: await userRepository.getUserById(id)
});

export const register = async ({ password, ...userData }) => {
  const newUser = await userRepository.addUser({
    ...userData,
    password: await encrypt(password)
  });
  return login(newUser);
};

export const changeUser = async (userId, { email, username, status, newPassword }) => {
  const objToSend = newPassword !== undefined
    ? { email, username, status, password: await encrypt(newPassword) }
    : { email, username, status };
  return userRepository.updateById(userId, objToSend);
};

export const changePassword = async email => {
  const user = await userRepository.getByEmail(email);
  if (!user) return { error: true, message: 'User with this email doesn\'t exist' };
  const emailHash = sha256(email);
  const result = await resetPasswordRepository.getByEmail(email);
  if (!result) await resetPasswordRepository.create({ email, hash: emailHash });
  sendListByEmail(`To change password go to the link http://localhost:3000/reset:${emailHash}`, email);
  return { error: false, message: 'Password sent to you by email' };
};

export const resetPassword = async (newPassword, emailHash) => {
  const { dataValues } = await resetPasswordRepository.getByHash(emailHash);
  if (newPassword && dataValues) {
    const { dataValues: data } = await userRepository.getByEmail(dataValues.email);
    const res = await userRepository.updateById(data.id, { password: await encrypt(newPassword) });
    if (res) {
      await resetPasswordRepository.deleteById(dataValues.id);
      return {
        error: false,
        message: 'Password successfully changed'
      };
    }
  }
  return { error: true, message: 'Error try again' };
};
