import { getReactedPostPerson, getReactedCommentPerson } from '../../data/repositories/lookForPersonWhoReact';

export const getPersonWhoReactOnPost = async (postId, reactType) => getReactedPostPerson(postId, reactType);

export const getPersonWhoReactOnComment = async (commentId, reactType) => getReactedCommentPerson(commentId, reactType);
