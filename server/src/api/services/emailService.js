import mailer from '../../config/nodemailerConfig';
import { getUserById } from './userService';

export const sendListByEmail = async (reaction, email, subject = 'Someone react on your post. Check it!') => {
  const message = {
    to: email,
    subject,
    text: reaction
  };
  mailer(message);
};
export const sendListById = async (reaction, id) => {
  const { email } = await getUserById(id);
  const message = {
    to: email,
    subject: 'Someone react on your post. Check it!',
    text: reaction
  };
  mailer(message);
};
