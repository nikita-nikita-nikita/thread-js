import postRepository from '../../data/repositories/postRepository';
import postReactionRepository from '../../data/repositories/postReactionRepository';

export const getPosts = async filter => postRepository.getPosts(filter);
export const getPostById = id => postRepository.getPostById(id)
  .then(post => ({
    ...post.dataValues,
    comments: post.dataValues.comments.filter(comment => !comment.isDelete)
  }));

export const create = (userId, post) => postRepository.create({
  ...post,
  userId
});

export const setReaction = async (userId, { postId, isLike, isDislike }) => {
  // get the reaction if it exists else create it
  let reaction = await postReactionRepository.getPostReaction(userId, postId);
  if (!reaction) reaction = await postReactionRepository.create({ userId, postId, isLike: false, isDislike: false });
  // define callback, that depends on like and dislike params, that comes from request
  const updateOrNullify = react => ((react.isLike === isLike && react.isDislike === isDislike)
    ? postReactionRepository.updateById(react.id, { isLike: false, isDislike: false })
    : postReactionRepository.updateById(react.id, { isLike, isDislike }));
  const { dataValues: { isLike: currentLike, isDislike: currentDislike } } = await updateOrNullify(reaction);
  const { dataValues: { isLike: prevLike, isDislike: prevDislike } } = reaction;
  const post = postRepository.getPostById(postId);
  return { newState: { isLike: currentLike, isDislike: currentDislike },
    previousState: { isLike: prevLike, isDislike: prevDislike },
    post };
};

export const changePost = async (postId, newPost, userId) => {
  // eslint-disable-next-line no-unused-expressions
  const { dataValues } = (userId === newPost.user.id)
    ? await postRepository.updateById(postId, newPost)
    : await postRepository.getPostById(postId);
  return { ...newPost,
    body: dataValues.body,
    id: dataValues.id,
    isDelete: dataValues.isDelete,
    image: dataValues.image
  };
};
