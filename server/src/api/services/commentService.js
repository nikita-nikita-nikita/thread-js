import commentRepository from '../../data/repositories/commentRepository';
import commentReactionRepository from '../../data/repositories/commentReactionRepository';

export const create = (userId, comment) => commentRepository.create({
  ...comment,
  userId
});

export const update = async (userId, comment) => ((userId === comment.userId)
  ? commentRepository.updateById(comment.id, comment)
  : commentRepository.getById(comment.id)
);

export const getCommentById = id => commentRepository.getCommentById(id);

// eslint-disable-next-line no-unused-vars
export const setReaction = async (userId, { isLike, isDislike, commentId }) => {
  let reaction = await commentReactionRepository.getCommentReaction(userId, commentId);
  if (!reaction) {
    reaction = await commentReactionRepository.create({
      userId,
      commentId,
      isLike: false,
      isDislike: false });
  }
  const updateOrNullify = react => ((react.isLike === isLike && react.isDislike === isDislike)
    ? commentReactionRepository.updateById(react.id, { isLike: false, isDislike: false })
    : commentReactionRepository.updateById(react.id, { isLike, isDislike }));
  const { dataValues: { isLike: currentLike, isDislike: currentDislike } } = await updateOrNullify(reaction);
  const { dataValues: { isLike: prevLike, isDislike: prevDislike } } = reaction;
  return { newState: { isLike: currentLike, isDislike: currentDislike },
    previousState: { isLike: prevLike, isDislike: prevDislike },
    comment: reaction };
};

export const getAllCommentsFromPost = postId => commentRepository.getCommentsByPostId(postId);
