import nodemailer from 'nodemailer';
import env from '../env';

const transporter = nodemailer.createTransport({
  host: 'smtp.gmail.com',
  port: 587,
  secure: false,
  auth: {
    user: env.email.email,
    pass: env.email.password
  }
},
{
  from: `Mailer Test <${env.email.email}>`
});

const mailer = message => {
  // eslint-disable-next-line consistent-return
  transporter.sendMail(message, (error, info) => {
    // eslint-disable-next-line no-console
    if (error) return console.log(error);
    // eslint-disable-next-line no-console
    console.log('info sent: ', info);
  });
};

export default mailer;
