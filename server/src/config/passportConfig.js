import passport from 'passport';
import { Strategy as LocalStrategy } from 'passport-local';
import { Strategy as JwtStrategy, ExtractJwt } from 'passport-jwt';
import { secret } from './jwtConfig';
import userRepository from '../data/repositories/userRepository';
import { compare } from '../helpers/cryptoHelper';

const options = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: secret
};

passport.use(
  'login',
  new LocalStrategy({ usernameField: 'email' }, async (email, password, done) => {
    try {
      const user = await userRepository.getByEmail(email);
      if (!user) {
        return done({ status: 401, message: 'Incorrect email.' }, false);
      }

      return await compare(password, user.password)
        ? done(null, user)
        : done({ status: 401, message: 'Passwords do not match.' }, null, false);
    } catch (err) {
      return done(err);
    }
  })
);
passport.use(
  'change',
  new LocalStrategy({ passReqToCallback: true, passwordField: 'oldPassword' },
    async ({ body: { status, email, newPassword }, user }, username, oldPassword, done) => {
      try {
        const {
          username: currentUsername,
          email: currentEmail,
          password
        } = await userRepository.getUserById(user.dataValues.id);
        if (!await compare(oldPassword, password)) {
          return done({ status: 401, message: 'Enter correct old password' }, false);
        }
        const userByEmail = await userRepository.getByEmail(email);
        if (userByEmail && currentEmail !== email) {
          return done({ status: 401, message: 'Email already taken.' }, false);
        }
        const userByUsername = await userRepository.getByUsername(username);
        if (userByUsername && currentUsername !== username) {
          return done({ status: 401, message: 'Username already taken.' }, false);
        }
        return done(null, { email, username, status, id: user.dataValues.id, password: newPassword });
      } catch (err) {
        return done(err);
      }
    })
);

passport.use(
  'register',
  new LocalStrategy(
    { passReqToCallback: true },
    async ({ body: { email } }, username, password, done) => {
      try {
        const userByEmail = await userRepository.getByEmail(email);
        if (userByEmail) {
          return done({ status: 401, message: 'Email is already taken.' }, null);
        }

        return await userRepository.getByUsername(username)
          ? done({ status: 401, message: 'Username is already taken.' }, null)
          : done(null, { email, username, password });
      } catch (err) {
        return done(err);
      }
    }
  )
);

passport.use(new JwtStrategy(options, async ({ id }, done) => {
  try {
    const user = await userRepository.getById(id);
    return user ? done(null, user) : done({ status: 401, message: 'Token is invalid.' }, null);
  } catch (err) {
    return done(err);
  }
}));
