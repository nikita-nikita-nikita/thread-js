export default [
  '/auth/login',
  '/auth/register',
  '/auth/change',
  '/auth/reset'
];
